import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import {BASE_URL, HEIGHT, WIDTH} from '../constants';
import * as RootNavigation from '../RootNavigation';
import api from '../helper/api';

const Discover = () => {
  const [restaurants, setRestaurants] = useState([]);
  const [cuisines, setCuisines] = useState([
    {
      _id: '6616d2caca15e562b7f1e09b',
      name: 'Banh Cuon',
      imagePath: '/images/cuisines/banh-cuon.jpg',
      __v: 0,
    },
    {
      _id: '6616d2caca15e562b7f1e09c',
      name: 'Banh Mi',
      imagePath: '/images/cuisines/banh-mi.jpg',
      __v: 0,
    },
    {
      _id: '6616d2caca15e562b7f1e09d',
      name: 'Bun Bo Hue',
      imagePath: '/images/cuisines/bun-bo-hue.jpg',
      __v: 0,
    },
    {
      _id: '6616d2caca15e562b7f1e09e',
      name: 'Bun Cha',
      imagePath: '/images/cuisines/bun-cha.jpg',
      __v: 0,
    },
  ]);
  const handleRestaurantPress = _id => {
    RootNavigation.navigate('RestaurantDetail', {_id});
  };

  useEffect(() => {
    const fetchRestaurants = async () => {
      try {
        const response = await api.getRestaurants();
        setRestaurants([...response]);
      } catch (error) {
        console.error(error);
      }
    };
    fetchRestaurants();

    return () => {
      setRestaurants([]);
    };
  }, []);

  return (
    <ScrollView style={styles.flex1}>
      {/* Filter */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'stretch',
          height: HEIGHT * 0.05,
          margin: WIDTH * 0.02,
          marginTop: WIDTH * 0.05,
        }}>
        <TextInput
          style={{
            flex: 1,
            backgroundColor: '#e5e5e5',
            borderRadius: WIDTH * 0.02,
            paddingHorizontal: WIDTH * 0.04,
            marginRight: WIDTH * 0.02,
          }}
          textAlignVertical="center"
          placeholder="Search"
        />
        <TouchableOpacity
          style={{
            width: HEIGHT * 0.05,
            height: HEIGHT * 0.05,
            borderRadius: WIDTH * 0.02,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#f43f5e',
            transform: [{rotate: '90deg'}],
          }}>
          <Entypo name="sound-mix" size={WIDTH * 0.05} color="white" />
        </TouchableOpacity>
      </View>
      {/* recommend */}
      <View
        style={{
          margin: WIDTH * 0.02,
          marginTop: WIDTH * 0.05,
        }}>
        <Text
          style={{
            fontSize: WIDTH * 0.05,
            fontWeight: 'bold',
            color: '#000',
          }}>
          Recommended for you
        </Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{
            marginTop: WIDTH * 0.02,
          }}>
          <View style={{flexDirection: 'row'}}>
            {restaurants.map(restaurant => (
              <TouchableOpacity
                key={restaurant._id}
                onPress={() => handleRestaurantPress(restaurant._id)}
                style={{
                  width: WIDTH * 0.6,
                  height: HEIGHT * 0.3,
                  marginRight: WIDTH * 0.02,
                  borderRadius: WIDTH * 0.02,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{
                    uri: BASE_URL + restaurant.imagePath,
                  }}
                  style={{
                    width: '100%',
                    height: '100%',
                    resizeMode: 'cover',
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    padding: WIDTH * 0.02,
                  }}>
                  <Text
                    style={{
                      fontSize: WIDTH * 0.04,
                      fontWeight: 'bold',
                      color: '#fff',
                    }}>
                    {restaurant.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: WIDTH * 0.03,
                      color: '#fff',
                    }}>
                    {restaurant.address}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
      {/* Cuisines */}
      <View
        style={{
          margin: WIDTH * 0.02,
          marginTop: WIDTH * 0.05,
        }}>
        <Text
          style={{
            fontSize: WIDTH * 0.05,
            fontWeight: 'bold',
            color: '#000',
          }}>
          Cuisines
        </Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{
            marginTop: WIDTH * 0.02,
          }}>
          <View style={{flexDirection: 'row'}}>
            {cuisines.map(cuisine => (
              <TouchableOpacity
                key={cuisine._id}
                style={{
                  width: WIDTH * 0.3,
                  height: HEIGHT * 0.15,
                  marginRight: WIDTH * 0.02,
                  borderRadius: WIDTH * 0.02,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{
                    uri: 'https://images.immediate.co.uk/production/volatile/sites/30/2015/02/Next-level-paella-f11ee26.jpg',
                    // uri: BASE_URL + cuisine.imagePath,
                  }}
                  style={{
                    width: '100%',
                    height: '100%',
                    resizeMode: 'cover',
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    padding: WIDTH * 0.02,
                  }}>
                  <Text
                    style={{
                      fontSize: WIDTH * 0.04,
                      fontWeight: 'bold',
                      color: '#fff',
                    }}>
                    {cuisine.name}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
      {/* Nearby */}
      <View
        style={{
          margin: WIDTH * 0.02,
          marginTop: WIDTH * 0.05,
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text
            style={{
              fontSize: WIDTH * 0.05,
              fontWeight: 'bold',
              color: '#000',
            }}>
            Nearby
          </Text>
          <Text style={{fontSize: WIDTH * 0.04, color: '#f43f5e'}}>
            View all
          </Text>
        </View>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{
            marginTop: WIDTH * 0.02,
          }}>
          <View style={{flexDirection: 'row'}}>
            {restaurants.map(restaurant => (
              <TouchableOpacity
                onPress={() => handleRestaurantPress(restaurant._id)}
                key={restaurant._id}
                style={{
                  width: WIDTH * 0.6,
                  height: HEIGHT * 0.3,
                  marginRight: WIDTH * 0.02,
                  borderRadius: WIDTH * 0.02,
                  overflow: 'hidden',
                }}>
                <Image
                  source={{
                    uri: BASE_URL + restaurant.imagePath,
                  }}
                  style={{
                    width: '100%',
                    height: '100%',
                    resizeMode: 'cover',
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    bottom: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    padding: WIDTH * 0.02,
                  }}>
                  <Text
                    style={{
                      fontSize: WIDTH * 0.04,
                      fontWeight: 'bold',
                      color: '#fff',
                    }}>
                    {restaurant.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: WIDTH * 0.03,
                      color: '#fff',
                    }}>
                    {restaurant.address}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>
    </ScrollView>
  );
};

export default Discover;

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  m2: {
    margin: WIDTH * 0.02,
  },
  justifyBetween: {
    justifyContent: 'space-between',
  },
  alignCenter: {
    alignItems: 'center',
  },
  bgGray200: {
    backgroundColor: '#e5e7eb',
  },
  textBase: {
    fontSize: WIDTH * 0.04,
  },
});
