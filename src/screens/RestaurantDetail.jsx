import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';
import {BASE_URL, HEIGHT, WIDTH} from '../constants';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as RootNavigation from '../RootNavigation';
import api from '../helper/api';

const restaurant = {
  _id: '6616d2caca15e562b7f1e097',
  name: 'Banh Cuon Thien Thanh (Houston, Texas)',
  rate: 4,
  address: '11210 Bellaire Blvd #140, Houston, TX 77072, Hoa Kỳ',
  openHours: '08:30 – 19:00',
  imagePath: '/images/restaurants/nha-hang-banh-cuon-thien-thanh.jpg',
  __v: 0,
};

const dishes = [
  {
    _id: '6616d2caca15e562b7f1e09d',
    restaurantId: '6616d2caca15e562b7f1e097',
    name: 'Bánh Cuốn Thanh Thanh',
    price: 30000,
    imagePath: '/images/dishes/banh-cuon-thien-thanh-huoston.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09d',
    restaurantId: '6616d2caca15e562b7f1e097',
    name: 'Bánh Cuốn Thanh Thanh',
    price: 30000,
    imagePath: '/images/dishes/banh-cuon-thien-thanh-huoston.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09d',
    restaurantId: '6616d2caca15e562b7f1e097',
    name: 'Bánh Cuốn Thanh Thanh',
    price: 30000,
    imagePath: '/images/dishes/banh-cuon-thien-thanh-huoston.jpg',
    __v: 0,
  },
];

const reviews = [
  {
    _id: '6616d2caca15e562b7f1e0a4',
    restaurantId: '6616d2caca15e562b7f1e09a',
    comment: 'Tuyệt vời!',
    reviewerName: 'Nguyên',
    rating: 4,
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e0a4',
    restaurantId: '6616d2caca15e562b7f1e09a',
    comment: 'Tuyệt vời!',
    reviewerName: 'Nguyên',
    rating: 4,
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e0a4',
    restaurantId: '6616d2caca15e562b7f1e09a',
    comment: 'Tuyệt vời!',
    reviewerName: 'Nguyên',
    rating: 4,
    __v: 0,
  },
];

const RestaurantDetail = ({route}) => {
  const _id = useMemo(() => route.params._id, [route.params._id]);
  const [restaurant, setRestaurant] = useState({});
  const [dishes, setDishes] = useState([]);
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    const fetchRestaurant = async () => {
      try {
        const response = await api.getRestaurant(_id);
        setRestaurant({...response});
      } catch (error) {
        console.error(error);
      }
    };
    fetchRestaurant();

    const fetchDishes = async () => {
      try {
        const response = await api.getDishes(_id);
        setDishes([...response]);
      } catch (error) {
        console.error(error);
      }
    };

    fetchDishes();

    const fetchReviews = async () => {
      try {
        const response = await api.getReviews(_id);
        setReviews([...response]);
      } catch (error) {
        console.error(error);
      }
    };

    fetchReviews();

    return () => {
      setRestaurant({});
      setDishes([]);
      setReviews([]);
    };
  }, [_id]);

  const [isTabActive, setIsTabActive] = useState(0);
  const handleGoBack = () => {
    RootNavigation.goBack();
  };

  const Detail = () => {
    return (
      <View style={{padding: WIDTH * 0.02, flex: 1}}>
        <View style={{marginBottom: WIDTH * 0.02, flex: 1}}>
          <View style={{marginBottom: WIDTH * 0.02, flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: WIDTH * 0.04,
                fontWeight: 'bold',
                width: WIDTH * 0.4,
              }}>
              Open Hours
            </Text>
            <Text style={{flex: 1, fontSize: WIDTH * 0.04, textAlign: 'right'}}>
              {restaurant.openHours}
            </Text>
          </View>
          <View style={{marginBottom: WIDTH * 0.02, flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: WIDTH * 0.04,
                fontWeight: 'bold',
                width: WIDTH * 0.4,
              }}>
              Address
            </Text>
            <Text style={{flex: 1, fontSize: WIDTH * 0.04, textAlign: 'right'}}>
              {restaurant.address}
            </Text>
          </View>
          <View>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: WIDTH * 0.02,
                marginTop: WIDTH * 0.02,
              }}>
              <Text
                style={{
                  fontSize: WIDTH * 0.04,
                  fontWeight: 'bold',
                  marginBottom: WIDTH * 0.02,
                }}>
                Dishes
              </Text>
              <Text
                style={{
                  flex: 1,
                  fontSize: WIDTH * 0.04,
                  textAlign: 'right',
                  color: '#f43f5e',
                }}>
                View all
              </Text>
            </View>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {dishes.map((dish, index) => (
                <View key={index} style={{marginRight: WIDTH * 0.02}}>
                  <Image
                    source={{
                      uri: 'https://images.immediate.co.uk/production/volatile/sites/30/2015/02/Next-level-paella-f11ee26.jpg',
                      // uri: BASE_URL + dish.imagePath
                    }}
                    style={{
                      height: WIDTH * 0.4,
                      width: WIDTH * 0.4,
                      resizeMode: 'cover',
                    }}
                  />
                  <Text style={{fontSize: WIDTH * 0.04}}>{dish.name}</Text>
                  <Text style={{fontSize: WIDTH * 0.04}}>{dish.price}</Text>
                </View>
              ))}
            </ScrollView>
          </View>
        </View>
        {/* book a table button */}
        <TouchableOpacity
          style={{
            backgroundColor: '#f43f5e',
            padding: WIDTH * 0.03,
            borderRadius: WIDTH * 0.02,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>Book a table</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const Reviews = () => {
    return (
      <View style={{padding: WIDTH * 0.02, flex: 1}}>
        <View style={{marginBottom: WIDTH * 0.02, flex: 1}}>
          {reviews.map((review, index) => (
            <View key={index} style={{marginBottom: WIDTH * 0.02}}>
              <Text style={{fontSize: WIDTH * 0.04, fontWeight: 'bold'}}>
                {review.reviewerName}
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <FontAwesome name="star" size={WIDTH * 0.04} color="#FFD700" />
                <Text
                  style={{fontSize: WIDTH * 0.04, marginLeft: WIDTH * 0.02}}>
                  {review.rating}
                </Text>
              </View>
              <Text style={{fontSize: WIDTH * 0.04}}>{review.comment}</Text>
            </View>
          ))}
        </View>
        {/* Leave a review button */}
        <TouchableOpacity
          style={{
            backgroundColor: '#f43f5e',
            padding: WIDTH * 0.03,
            borderRadius: WIDTH * 0.02,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white'}}>Leave a review</Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={{flex: 1, position: 'relative'}}>
      <View style={{position: 'relative'}}>
        <Image
          source={{uri: BASE_URL + restaurant.imagePath}}
          style={{height: WIDTH * 0.6, width: WIDTH, resizeMode: 'cover'}}
        />
        <View
          style={{
            position: 'absolute',
            top: HEIGHT * 0.02,
            left: WIDTH * 0.02,
            right: WIDTH * 0.02,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            onPress={handleGoBack}
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0.4)',
              width: WIDTH * 0.1,
              height: WIDTH * 0.1,
              borderRadius: WIDTH * 0.05,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <FontAwesome name="angle-left" size={WIDTH * 0.06} color="white" />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: 'rgba(0, 0, 0, 0.4)',
              width: WIDTH * 0.1,
              height: WIDTH * 0.1,
              borderRadius: WIDTH * 0.05,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <FontAwesome name="bookmark-o" size={WIDTH * 0.06} color="white" />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{padding: WIDTH * 0.02}}>
        <Text style={{fontSize: WIDTH * 0.05, fontWeight: 'bold'}}>
          {restaurant.name}
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: WIDTH * 0.02,
          }}>
          <FontAwesome name="star" size={WIDTH * 0.04} color="#FFD700" />
          <Text style={{fontSize: WIDTH * 0.04, marginLeft: WIDTH * 0.02}}>
            {restaurant.rate}
          </Text>
        </View>
      </View>
      {/* tabs */}
      <View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity
            style={{
              padding: WIDTH * 0.02,
              borderBottomWidth: isTabActive === 0 ? 2 : 0,
              borderBottomColor: isTabActive === 0 ? 'black' : 'white',
            }}
            onPress={() => setIsTabActive(0)}>
            <Text style={{fontSize: WIDTH * 0.04}}>Detail</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              padding: WIDTH * 0.02,
              borderBottomWidth: isTabActive === 1 ? 2 : 0,
              borderBottomColor: isTabActive === 1 ? 'black' : 'white',
            }}
            onPress={() => setIsTabActive(1)}>
            <Text style={{fontSize: WIDTH * 0.04}}>Reviews</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* content */}
      {isTabActive === 0 ? <Detail /> : <Reviews />}
    </View>
  );
};

export default RestaurantDetail;

const styles = StyleSheet.create({});
