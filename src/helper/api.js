import axios from 'axios';
import {BASE_URL} from '../constants';
import _ from 'lodash';

const instance = axios.create({
  baseURL: BASE_URL,
  timeout: 30000,
});

instance.interceptors.request.use(
  config => {
    __DEV__ && console.log('Request Interceptor', JSON.stringify(config));
    return config;
  },
  error => {
    __DEV__ && console.error('Request Interceptor', error);
    return Promise.reject(error);
  },
);

instance.interceptors.response.use(
  response => {
    __DEV__ &&
      console.log('Response Interceptor', JSON.stringify(response.data));
    return response;
  },
  error => {
    __DEV__ && console.error('Response Interceptor', error);
    return Promise.reject(error);
  },
);

const api = {
  getRestaurants: async () => {
    try {
      const response = await instance.get('/restaurants');
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },

  getRestaurant: async _id => {
    try {
      const response = await instance.get(`/restaurants/${_id}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },

  getDishes: async _id => {
    try {
      const response = await instance.get(`/restaurants/${_id}/dishes`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  },

  getReviews: async _id => {
    try {
      const response = await instance.get(`/restaurants/${_id}/reviews`);
      if (_.isArray(response.data)) {
        return response.data;
      } else if (_.isObject(response.data)) {
        return [response.data];
      } else {
        return [];
      }
    } catch (error) {
      console.error(error);
    }
  },
};

export default api;
