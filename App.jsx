import {
  Dimensions,
  Image,
  Pressable,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {navigationRef} from './src/RootNavigation';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import RestaurantDetail from './src/screens/RestaurantDetail';
import {BASE_URL, HEIGHT, WIDTH} from './src/constants';
import Discover from './src/screens/Discover';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const restaurants = [
  {
    _id: '6616d2caca15e562b7f1e097',
    name: 'Banh Cuon Thien Thanh (Houston, Texas)',
    rate: 4,
    address: '11210 Bellaire Blvd #140, Houston, TX 77072, Hoa Kỳ',
    openHours: '08:30 – 19:00',
    imagePath: '/images/restaurants/nha-hang-banh-cuon-thien-thanh.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e098',
    name: 'Bolero (Brooklyn, New York)',
    rate: 4.3,
    address: '177 Bedford Ave, Brooklyn, NY 11211, Hoa Kỳ',
    openHours: '08:30 – 19:00',
    imagePath: '/images/restaurants/nha-hang-bolero.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e099',
    name: 'Brodard Restaurant (Fountain Valley, California)',
    rate: 5,
    address: '16105 Brookhurst St, Fountain Valley, CA 92708, Hoa Kỳ',
    openHours: '08:00 – 21:00',
    imagePath: '/images/restaurants/nha-hang-brodard.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09a',
    name: 'Phở Gà Thanh Thanh (Philadelphia, Pennsylvania)',
    rate: 3,
    address: '1100 Washington Ave, Philadelphia, PA 19147, Hoa Kỳ',
    openHours: '10:00 – 18:30',
    imagePath: '/images/restaurants/nha-hang-pho-ga-thanh-thanh.jpg',
    __v: 0,
  },
];

const cuisines = [
  {
    _id: '6616d2caca15e562b7f1e09b',
    name: 'Banh Cuon',
    imagePath: '/images/cuisines/banh-cuon.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09c',
    name: 'Banh Mi',
    imagePath: '/images/cuisines/banh-mi.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09d',
    name: 'Bun Bo Hue',
    imagePath: '/images/cuisines/bun-bo-hue.jpg',
    __v: 0,
  },
  {
    _id: '6616d2caca15e562b7f1e09e',
    name: 'Bun Cha',
    imagePath: '/images/cuisines/bun-cha.jpg',
    __v: 0,
  },
];

const BellBtn = () => {
  return (
    <TouchableOpacity
      style={{
        padding: WIDTH * 0.02,
      }}>
      <AntDesign name="bells" size={width * 0.05} color="black" />
    </TouchableOpacity>
  );
};

const NearbyScreen = () => {
  return (
    <View>
      <Text>NearbyScreen</Text>
    </View>
  );
};

const SavedScreen = () => {
  return (
    <View>
      <Text>SavedScreen</Text>
    </View>
  );
};

const ProfileScreen = () => {
  return (
    <View>
      <Text>ProfileScreen</Text>
    </View>
  );
};

const Stack = createNativeStackNavigator();

const Tab = createBottomTabNavigator();

const DiscoverIcon = ({color, size}) => (
  <FontAwesome name="compass" size={size} color={color} />
);

const NearbyIcon = ({color, size}) => (
  <FontAwesome name="map-o" size={size} color={color} />
);

const SavedIcon = ({color, size}) => (
  <FontAwesome name="bookmark-o" size={size} color={color} />
);

const ProfileIcon = ({color, size}) => (
  <FontAwesome name="user-o" size={size} color={color} />
);

const Init = () => {
  return (
    <Tab.Navigator initialRouteName="Discover">
      <Tab.Screen
        name="Discover"
        options={{
          title: 'Discover Restaurants',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          tabBarShowLabel: false,
          headerRight: BellBtn,
          tabBarIcon: DiscoverIcon,
        }}
        component={Discover}
      />
      <Tab.Screen
        name="Nearby"
        options={{
          title: 'Nearby',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          tabBarShowLabel: false,
          headerRight: BellBtn,
          tabBarIcon: NearbyIcon,
        }}
        component={NearbyScreen}
      />
      <Tab.Screen
        name="Saved"
        options={{
          title: 'Saved',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          tabBarShowLabel: false,
          headerRight: BellBtn,
          tabBarIcon: SavedIcon,
        }}
        component={SavedScreen}
      />
      <Tab.Screen
        name="Profile"
        options={{
          title: 'Profile',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
          tabBarShowLabel: false,
          headerRight: BellBtn,
          tabBarIcon: ProfileIcon,
        }}
        component={ProfileScreen}
      />
    </Tab.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator>
        <Stack.Screen
          name="Init"
          component={Init}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RestaurantDetail"
          component={RestaurantDetail}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  m2: {
    margin: WIDTH * 0.02,
  },
  justifyBetween: {
    justifyContent: 'space-between',
  },
  alignCenter: {
    alignItems: 'center',
  },
  bgGray200: {
    backgroundColor: '#e5e7eb',
  },
  textBase: {
    fontSize: WIDTH * 0.04,
  },
});
